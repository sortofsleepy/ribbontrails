(dotimes [i (- (.-LEN this) 1)]
  (let [vertices (.-array (.getAttribute (.-geometry this) "position"))]

      ;;copy over first set of verts
      (let [index (* 2 (- i 1)) ;; index to start copying over values from
            v1 (aget vertices index) ;; first value to copy over
            v2 (aget vertices (+ 1 index));; second value to copy over
            v3 (aget vertices (+ 2 index));; third value to copy over
            s1 (* i 2) ;; starting index to change value
            s2 (+ 1 s1);; second starting index to change value
            s3 (+ 2 s1)];; third starting index to change value
            (aset vertices s1 v1)
            (aset vertices s2 v2)
            (aset vertices s3 v3)
            )


            ;;copy over first set of verts
        (let [index (* (+ 2 1) (- i 1)) ;; index to start copying over values from
              v1 (aget vertices index) ;; first value to copy over
              v2 (aget vertices (+ 1 index));; second value to copy over
              v3 (aget vertices (+ 2 index));; third value to copy over
              s1 (* i (+ 1 2)) ;; starting index to change value
              s2 (+ 1 s1);; second starting index to change value
              s3 (+ 2 s1)];; third starting index to change value
              (aset vertices s1 v1)
              (aset vertices s2 v2)
              (aset vertices s3 v3)
            )

    ))
