 (ns ribbontrails.ribbon
  (:require
   [ribbontrails.global :as global]
   [thi.ng.typedarrays.core :as tarrays]
   [thi.ng.math.simplexnoise :as noise]
   [ribbontrails.math :as math]))


(defonce SimplexNoise (js/window.SimplexNoise.))
(defonce color (js/THREE.Color.))
(defonce hue 0)
(def lightness (math/randomRange 0.2 0.6))
(def sat (math/randomRange 0.6 1))

(defn flattenIndices [vecarray]
  "Flattens indices array so that we can add it to the buffer geometry"
  (let [finalArray #js []]
    (dotimes [i (.-length vecarray)]
      (.push finalArray (.-a (aget vecarray i)))
      (.push finalArray (.-b (aget vecarray i)))
            (.push finalArray 0)
      (.push finalArray (.-a (aget vecarray i)))
      )finalArray))

(defn updatePositions [geo verts]
  "Takes care of updating the positions attribute of the geometry"

  ;; get reference to position attribute
  (let [positionAttrib (.getAttribute geo "position")]
    ;;get reference to vertices array
    (let [vertices (.-array positionAttrib)]

      ;;loop through array setting the values and return the array
      ;; note that there isn't a explicit return statement
      (dotimes [i (.-length vertices)]
        (aset vertices i (aget verts i))))))



(deftype Ribbon [attractor]
  Object
  (init [this]
        (aset this "LEN" 40)
        (aset this "velocity" (js/THREE.Vector3.))
        (aset this "ribbonWidth" (math/randomRange 2 12))
        (aset this "head" (js/THREE.Vector3.))
        (aset this "tail" (js/THREE.Vector3.))
        (aset this "speed" (math/randomRange 5 20))
        (aset this "clumpiness" 0.8)
        (aset this "ribbonSpeed" 1)
        (aset this "attractorActive" true)
        (aset this "attractForce" (js/THREE.Vector3.))
        this)

        (reset [this]
          ;; forward declare some local vars
          (let [verts (.-vertices this)
                noiseSeperation global/noiseSeperation]

            ;; set id attribute
            (aset this "id" (* (js/Math.random) noiseSeperation))


            (let [clumpiness (.-clumpiness this)
                  head (.-head this)
                  emiterId (math/randomInt 0 (-  global/EMITTER_COUNT 1))
                  randBoundsVector (math/randVector3 global/BOUNDS)
                  randVec3 (math/randVector3 global/startRange)]

                  ;; if statement. 2nd () runs if statement is true, 3rd one runs if false
                  (if (< (js/Math.random) clumpiness)
                    (.addVectors head (aget global/emitters emiterId) randVec3)
                    (.copy head randBoundsVector)))

            (.copy (.-tail this) (.-head this))

            ;; loop through and copy the head
            (dotimes [i (.-LEN this)]
              (let [v1 (aget verts (* i 2))
                    head (.-head this)
                    v2 (aget verts (+ (* i 2) 1))]
                    (.copy v1 head)
                    (.copy v2 head)))

            (set! global/resetFlat #js [])
            (dotimes [i (.-length verts)]
              (.push global/resetFlat (.-x (aget verts i)) (.-y (aget verts i)) (.-z (aget verts i)) ))

              (updatePositions (.-geometry this) global/resetFlat)
                (let [positionAttrib (.getAttribute (.-geometry this) "position")]
                  (aset positionAttrib "needsUpdate" true))
            ))

            (update [this]
              (let [verts (.-vertices this)
                    tail (.-tail this)
                    head (.-head this)
                    mainVec global/mainVec
                    velocity (.-velocity this)
                    simplexNoise SimplexNoise
                    id (.-id this)
                    tangent global/tangent
                    up global/up
                    noiseTime global/noiseTime
                    ribbonWidth (.-ribbonWidth this)
                    noiseScale global/noiseScale
                    normal global/normal
                    BOUNDS global/BOUNDS
                    attractor (.-attractor this)
                    geometry (.-geometry this)]

                    (.copy tail head)
                    (.copy mainVec head)
                    (.divideScalar mainVec noiseScale)


                    (let [timeX (+ 0 noiseTime (.-id this))
                          timeY (+ 50 noiseTime (.-id this))
                          timeZ (+ 100 noiseTime (.-id this))
                          speed (* (.-speed this) (.-ribbonSpeed this))
                          _vec global/mainVec
                          velocity (.-velocity this)]



                          (aset velocity "x" (* (.noise4d SimplexNoise (aget _vec "x") (aget _vec "y") (aget _vec "z") timeX) speed))
                          (aset velocity "y" (* (.noise4d SimplexNoise (aget _vec "x") (aget _vec "y") (aget _vec "z") timeY) speed))
                          (aset velocity "z" (* (.noise4d SimplexNoise (aget _vec "x") (aget _vec "y") (aget _vec "z") timeZ) speed))

                          ;;(.add velocity _vec)
                          )



                    (.add head velocity)

                   ;; make sure head is within bounds
                   (if (> (.-x head) BOUNDS)
                    (.reset this))
                    (if (< (.-x head) (* -1 BOUNDS))
                     (.reset this))
                     (if (> (.-y head) BOUNDS)
                        (.reset this))
                      (if (< (.-y head) (* -1 BOUNDS))
                       (.reset this))
                      (if (> (.-z head) BOUNDS)
                       (.reset this))
                       (if (< (.-z head) (* -1 BOUNDS))
                        (.reset this))

                    (.subVectors tangent head tail)
                    (.normalize tangent)

                    (.crossVectors mainVec tangent up)
                    (.normalize mainVec)

                    (.crossVectors normal tangent mainVec)
                    (.multiplyScalar normal ribbonWidth)

                    ;; add attractive force to velocity
                    (let [aForce (.seek attractor mainVec velocity)]
                    ;;(aset this "attractForce" aForce)
                     (.add head aForce)
                     (.add tail aForce)
                     (.add normal aForce)
                   )
                    ;; build new set
                  (loop [i (- (.-LEN this) 1)]
                    (when (> i 0)
                    (let [v (aget verts (* i 2))
                          c1 (aget verts (* (- i 1) 2))]
                          (.copy v c1))

                          (let [v2 (aget verts (+ (* i 2) 1))
                                c2 (aget verts (+ (* (- i 1) 2) 1))]
                                (.copy v2 c2))
                     (recur (dec i))))

                       (.copy (aget verts 0) head)
                       (.add (aget verts 0) normal)

                       (.copy (aget verts 1) head)
                       (.sub (aget verts 1) normal)




                       (set! global/updateFlat #js [])
                       (dotimes [i (.-length verts)]
                          (let [vert (aget verts i)]
                            (.push global/updateFlat (.-x vert))
                            (.push global/updateFlat (.-y vert))
                            (.push global/updateFlat (.-z vert))
                            )

                          )

                          (updatePositions (.-geometry this) global/updateFlat)
                            (let [positionAttrib (.getAttribute (.-geometry this) "position")]
                              (let [vertices (.-array positionAttrib)]
                              (aset positionAttrib "needsUpdate" true)))

                    ))

        (buildMesh [this]
          (let [geo (js/THREE.BufferGeometry.)
                vertices (tarrays/float32 (* 6 (.-LEN this)))
                colors (tarrays/float32 (* 3 (.-LEN this)))
                indices (tarrays/uint16 (* 3 (.-LEN this)))
                verts #js []
                indexes #js []]

                ;; flatten and set vertices
                (dotimes [i (.-LEN this)]
                  (.push verts (js/THREE.Vector3. 0 0 0))
                  (.push verts (js/THREE.Vector3. 0 0 0)))

                (let [verticesFlat (math/flattenArray verts)]
                  (dotimes [i (.-length verticesFlat)]
                    (aset vertices i (aget verticesFlat i))))
                      ;; build indices
                      (dotimes [i (.-LEN this)]
                        (.push indexes (js/THREE.Face3. (* 2 i) (+ 1 (* i 2)) (+ 2 (* i 2))))
                        (.push indexes (js/THREE.Face3. (+ 1 (* i 2)) (+ 3 (* i 2)) (+ 2 (* i 2)))))
                          ;;flatten and set indices
                          (let [indicesFlat (flattenIndices indexes)]

                      (dotimes [i (.-length indicesFlat)]
                        (aset indices i (aget indicesFlat i))))

                           ;; set colors first
                           (set! hue (/ (/ (.-head.x this) (/ (.-BOUNDS this) 2)) (+ 2 0.5)))
                           (if (< (js/Math.random) 0.1)
                             (set! hue (js/Math.random)))




                             ;; build new set
                                (set! hue (/ (/ (.-head.x this) (/ (.-BOUNDS this) 2)) (+ 2 0.5)))

                                (if (< (js/Math.random) 0.1)
                                  (set! hue (js/Math.random)))


                           (loop [i 0]
                             (when (< i (.-length colors))
                             (let [p1 (/ (- 1 i) (.-LEN this))
                                   p2 (/ lightness 4)]
                                   (let [p3 (/ 3 4)
                                     p4 (* lightness p3)]
                                     (let [lightVal (+ (* p1 p2) (* p3 p4))]
                                      ()
                                      ;;(.setHSL color (/ i (.-length colors)) 1.0 0.5)
        (.setHSL color hue sat lightVal)
                                      (aset colors i (.-r color))
                                      (aset colors (+ i 1) (.-g color))
                                      (aset colors (+ i 2) (.-b color))



                                      )))

                              (recur (+ i 3))))


                             (let [colorAttrib (js/THREE.BufferAttribute. colors 3)]
                               (.addAttribute geo "color" colorAttrib))

                               (aset this "colorArray" colors)


                        (let [positionAttrib (js/THREE.BufferAttribute. vertices 3)]
                          (.addAttribute geo "position" positionAttrib))
                          (let [indexAttrib (js/THREE.BufferAttribute. indices 1)]
                            (.setIndex geo indexAttrib))


                            (aset this "vertices" verts)
                            (aset this "vertlen" (.-length verts))
                            (aset this "geometry" geo)



                            ;; build the mesh
                            (let [material (js/THREE.ShaderMaterial. (clj->js
                              { :blending js/THREE.AdditiveBlending
                                :transparent false
                                :depthWrite false
                                :uniforms (clj->js {
                                  :time (clj->js {
                                    :type "f"
                                    :value 0.0
                                    })
                                  })
                                :vertexShader (.-textContent (js/document.getElementById "vertexShader"))
                                :fragmentShader (.-textContent (js/document.getElementById "fragmentShader")) }))]
                            (aset this "mesh" (js/THREE.Mesh. geo material))
                            (aset this "material" material)
                            )

                            )this)

    )




;;======== FUNCTIONS =========;;


(defn MakeRibbon [scene attractor]
  (let [rib (Ribbon. attractor)]

    (.init rib)
    (.buildMesh rib)
    (.reset rib)
    (.add scene (.-mesh rib))
    rib))
