(ns ribbontrails.math
  (:require
     [thi.ng.geom.core :as geom]
     [thi.ng.math.simplexnoise :as noise]
     [thi.ng.math.core :as math]
     [thi.ng.typedarrays.core :as tarrays]
     [thi.ng.geom.core.vector :as v :refer [vec2 vec3]]))



;; =========== THI.NG HELPERS =============== ;;
(defn getVecX [v]
  "Gets the x component of a thi.ng vector"
  (aget (.-buf v) 0))

(defn getVecY [v]
   "Gets the y component of a thi.ng vector"
  (aget (.-buf v) 1))

(defn getVecZ [v]
   "Gets the z component of a thi.ng vector"
  (aget (.-buf v) 2))

(defn randFloat [max]
  (noise/noise1 max))

(defn divideScalar [v scalar]
  "Divide all components of a thi.ng vec3 by a scalar value"
  (aset (.-buf v) 0 (/ scalar (getVecX v)))
  (aset (.-buf v) 1 (/ scalar (getVecX v)))
  (aset (.-buf v) 2 (/ scalar (getVecX v)))
  v)



(defn multiplyScalar [v scalar]
  "Multiply all components of a thi.ng vec3 by a scalar value"
  (aset (.-buf v) 0 (* scalar (getVecX v)))
  (aset (.-buf v) 1 (* scalar (getVecX v)))
  (aset (.-buf v) 2 (* scalar (getVecX v)))
  v)

(defn addScalar [v scalar]
  "Adds all components of a thi.ng vec3 by a scalar value"
  (aset (.-buf v) 0 (+ scalar (getVecX v)))
  (aset (.-buf v) 1 (+ scalar (getVecX v)))
  (aset (.-buf v) 2 (+ scalar (getVecX v)))
  v)

(defn subScalar [v scalar]
  "Subtracts all components of a thi.ng vec3 by a scalar value"
  (aset (.-buf v) 0 (- scalar (getVecX v)))
  (aset (.-buf v) 1 (- scalar (getVecX v)))
  (aset (.-buf v) 2 (- scalar (getVecX v)))
  v)

;; =========== GENERAL =============== ;;

(defn lineLength [x y x0 y0]
  (let [x (* (- x x0) x)
        y (* (- y y0) y)]
    (js/Math.sqrt (+ x y))))

(defn randomRange [min max]
  (let [v1 (* (js/Math.random) (- max min))]
    (+ min v1)))

(defn randomInt [min max]
  (let [s1 (+ min (js/Math.random))
        s2 (+ (- max min) 1)]
    (js/Math.floor (* s1 s2))))

;;Math.floor(min + Math.random() * (max - min + 1));
(defn mapValue [value min1 max1 min2 max2]
  ())

(defn normalize [value min max]
                 (let [toDivide (- value min)
                       divisor (- max min)]
                   (/ toDivide divisor)))

(defn lerp [value min max]
  (let [diff (- max min)]
    (+ min (* diff value))))

(defn randVector3 [range]
  (if (not= nil js/window.THREE)
    (js/THREE.Vector3. (randomRange (* -1 range) range) (randomRange (* -1 range) range) (randomRange (* -1 range) range))
    (vec3 (randomRange (* -1 range) range) (randomRange (* -1 range) range) (randomRange (* -1 range) range))
    ))

(defn vector3 []
  (if (not= nil js/window.THREE)
    (js/THREE.Vector3.)
    (vec3 0 0 0)
    ))


(defn constrain [amt low hight]
  "Constrains a value between two values"
  (if (< amt low)
    low
    (if (> amt hight)
      hight
      amt)))

(defn flattenArray [vecarray]
    "Takes a array of vectors and flatens things so that all of the values
    flow consecutively one after another, ie [x,y,z,x,y,z]. Assumes that your vector
    object has x y and z properties on it"
    (let [finalArray #js []]
      (dotimes [i (.-length vecarray)]
        (let [vec (nth vecarray i)]
          (if (= (type vec) js/THREE.Vector3)
            (let [reserve 0]
              (.push finalArray (.-x vec))
              (.push finalArray (.-y vec))
              (.push finalArray (.-z vec)))
              (.push finalArray vec))
            ))
finalArray))

(defn float32 [amountOrArray]
  "Builds a float32 array. Ensures that the array is in a flat state consisting of
  just values as opposed to a arrray of objects, then returns the new array"
  (if (= (type amountOrArray) js/Array)
    (let [flatArray (flattenArray amountOrArray)]
      (tarrays/float32 flatArray))
    (tarrays/float32 (* 3 amountOrArray))))

(defn toThingVector [threeVector]
  "Converts a Three.js vector to a Thi.ng vector"
  (vec3 (.-x threeVector) (.-y threeVector) (.-z threeVector) ))

(defn assignRandToArray [arr value]
  "Enables you to quickly populate a array with a random value.
  returns the newly populated array "
  (loop [i 0]
    (if (< i (.-length arr))
      (let [x i
            y (+ 1 i)
            z (+ 2 i)]
          (aset arr x (* js/Math.random value))
          (aset arr y (* js/Math.random value))
          (aset arr z (* js/Math.random value))
        (recur (+ 3 i)))
      ()))
      arr)


(defn assignToArray [arr cb]
  "This function loops through a flat array and allows you to set values in sets of 3 in a callback"
  (loop [i 0]
    (if (< i (.-length arr))
      (let [x i
            y (+ 1 i)
            z (+ 2 i)]
          (cb arr x y z)
        (recur (+ 3 i)))
      ()))
      arr)
