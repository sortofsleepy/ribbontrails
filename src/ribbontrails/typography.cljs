(ns ribbontrails.typography
  (:require
    [thi.ng.typedarrays.core :as tarrays]
    [ribbontrails.attractor :as attractor]))

(defn CreateAttractors [text]
  (let [geo (js/THREE.TextGeometry. text
    (clj->js {:size 70
              :height 20
              :curveSegments 4
              :bevelThickness 2
              :font "droid sans"}))]

    ;; build a visual reference to where all the attractors would be
    (let [buffGeo (js/THREE.BufferGeometry.)
          vertLen (.-vertices.length geo)
          verts (.-vertices geo)
          textVerts #js []
          colors (tarrays/float32 (* 3 vertLen))]

          ;; flatten vertices of TextGeometry
          (dotimes [i (.-vertices.length geo)]
            (let [vert (aget (.-vertices geo) i)]
              (.push textVerts (.-x vert) (.-y vert) (.-z vert))))

          ;; add positions
          (let [vertices (tarrays/float32 textVerts)]
            (let [positionAttrib (js/THREE.BufferAttribute. vertices 3)]
              (.addAttribute buffGeo "position" positionAttrib)))

          (let [mat (js/THREE.MeshBasicMaterial. (clj->js {:color 0xff0000}))]

            (let [pt (js/THREE.Points. buffGeo mat)
                  attractors #js []]

                ;; loop through vertices and create attractors
                (dotimes [i vertLen]
                  (let [attractor (attractor/makeAttractor (aget verts i) 1.0)]
                   (.push attractors attractor)))
                 {:mesh pt :attractors attractors})

          ))))
